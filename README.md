# View caching project

This is a Django project running on Django 2. See the `requirements.txt` for exact versions.

## Task One: Django Caching Middleware

The project has a Django middleware declared in `main/middleware.py`. It caches view
responses declared in a`CACHE_URLS` setting. The request path is used as the cache
key.

The cache is configured to use the file based cache system.

For testing purposes the main app has two views, `/employees` and `/static` which
test the caching functionality of the middleware. The views are just displaying
some random values as JSON which is ideal in quickly validating if the middleware
is working as expected.

Unit tests testing the middleware functionality are declared in `main/test_middleware.py`.

### Running locally

Clone the repo:

`git clone https://gitlab.com/erickmbwana/middle.git`

Move into the directory:

`cd middle`

Install the python packages, preferably in a separate virtual environment:

`pip install -r requirements.txt`

Apply migrations and start the development server and visit the views to see how
the middleware works.

`python manage.py migrate`

`python manage.py runserver`

You can run the unit tests as so:

`python manage.py test`

## Task 2: Docker Stack with Timescale and Postgres

The project declares a simplistic `docker-compose.yml` file which can be used to run the
project as a Docker swarm.

The Django project uses an environment variable `POSTGRES_PASSWORD` to connect to
the timescale database.

Update the `settings.py` with the follow database settings:

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
        'PASSWORD': os.getenv('POSTGRES_PASSWORD'),
    }
}
```


Assuming you have Docker and Docker Compose installed locally, you can run the
project thus(within this directory):

`docker-compose up`

Then visit `localhost:8000` on the local machine to access the dockerised Django
project.
