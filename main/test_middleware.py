import time

from django.core.cache import cache
from django.test import TestCase, override_settings

CACHE_MISS_URLS = [
    ('/employees', 1)
]

CACHE_HIT_URLS = [
    ('/employees', 10)
]

# Have separate cache for tests so that main cache is not cleared
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': 'testcache',
    }
}


class ViewCachingMiddlewareTestCase(TestCase):

    @override_settings(CACHES=CACHES)
    def setUp(self):
        super().setUp()

    def tearDown(self):
        cache.clear()

    @override_settings(CACHE_URLS=CACHE_MISS_URLS)
    def test_cache_miss(self):
        res1 = self.client.get('/employees')

        # sleep for 2 sec, cache is for 1 sec, that should be a miss
        # the 2 responses must have diff content
        time.sleep(2)

        res2 = self.client.get('/employees')

        self.assertNotEqual(res1.content, res2.content)

    @override_settings(CACHE_URLS=CACHE_HIT_URLS)
    def test_cache_hit(self):
        res1 = self.client.get('/employees')

        # cache is for 10 sec, that should be a hit, content must be same

        res2 = self.client.get('/employees')

        self.assertEqual(res1.content, res2.content)
