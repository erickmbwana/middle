import random

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.generic import View

from faker import Faker


faker = Faker()


class EmployeeView(View):

    def get(self, request, *args, **kwargs):
        data = {
            'first_name': faker.first_name(),
            'last_name': faker.last_name(),
            'age': random.randint(18, 49)
        }
        return JsonResponse(data)


@method_decorator(cache_page(60 * 1), name='dispatch')
class CachedView(View):

    def get(self, request, *args, **kwargs):
        data = {
            'first_name': faker.first_name(),
            'last_name': faker.last_name(),
            'age': random.randint(18, 49)
        }
        return JsonResponse(data)
