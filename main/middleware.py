from django.conf import settings
from django.core.cache import cache


class ViewCachingMiddleware(object):
    """
    A middleware for caching views.

    The views to be cached and the timeout are declared in the CACHE_URLS
    setting. The request path is used as the cache key.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Before processing, check cache, if key in return response
        paths = [path for path, _ in settings.CACHE_URLS]
        current_path = request.path

        # If current path is in our paths try to get the response from the
        # cache. If we get a cache miss process the response and add it in the
        # cache
        if current_path in paths:
            response = cache.get(current_path)
            if not response:
                # That was a miss, add it to the cache
                response = self.get_response(request)
                cache_configs = dict(settings.CACHE_URLS)
                timeout = cache_configs[current_path]
                cache.set(current_path, response, timeout=timeout)
        else:
            # if path is not in our specified paths, just process normally
            response = self.get_response(request)
        return response
